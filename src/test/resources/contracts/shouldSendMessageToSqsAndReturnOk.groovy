import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "should return even when number input is even"
    request {
        method POST()
        url("/sqs") {
            body("Hello World!")
        }
    }
    response {
        body("")
        status 200
    }
}
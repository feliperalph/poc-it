package poc.integration;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import poc.integration.configuration.config.AwsSqsTestConfig;

@Import(AwsSqsTestConfig.class)
@SpringBootTest
@ActiveProfiles({"test", "integration"})
public abstract class AbstractIntegrationTest {

}

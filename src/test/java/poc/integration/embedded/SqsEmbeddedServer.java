package poc.integration.embedded;

import com.amazonaws.services.sqs.AmazonSQSClient;
import lombok.Getter;
import lombok.extern.apachecommons.CommonsLog;
import org.elasticmq.rest.sqs.SQSRestServer;
import org.elasticmq.rest.sqs.SQSRestServerBuilder;
import org.springframework.context.Lifecycle;

import java.util.List;

@CommonsLog
public class SqsEmbeddedServer implements Lifecycle {

    private SQSRestServer sqsRestServer;

    @Getter
    private boolean running;
    @Getter
    private String url;

    @Override
    public void start() {
        if (isRunning()) {
            return;
        }
        log.info("Initializing ElasticMQ Embedded");
        this.sqsRestServer = SQSRestServerBuilder.withDynamicPort().start();
        this.url = "http://localhost:" + sqsRestServer.waitUntilStarted().localAddress().getPort();
        log.info("Started ElasticMQ Embedded at: " + this.url);
        running = true;
    }

    @Override
    public void stop() {
        if (!isRunning()) {
            return;
        }
        log.info("Shutting down ElasticMQ Embedded at: " + this.url);
        sqsRestServer.stopAndGetFuture();
        running = false;
    }

    public String createQueue(String queueName) {
        AmazonSQSClient sqsClient = new AmazonSQSClient().withEndpoint(url);
        var queueUrl = sqsClient.createQueue(queueName).getQueueUrl();
        log.info("Created queue in ElasticMQ: " + queueUrl);
        return queueUrl;
    }

    public List<String> listQueues() {
        AmazonSQSClient sqsClient = new AmazonSQSClient().withEndpoint(url);
        var queueUrls = sqsClient.listQueues().getQueueUrls();
        return queueUrls;
    }
}

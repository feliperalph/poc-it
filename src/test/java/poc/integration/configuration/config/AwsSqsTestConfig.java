package poc.integration.configuration.config;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.AmazonSQSAsyncClientBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.cloud.aws.messaging.config.SimpleMessageListenerContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import poc.integration.embedded.SqsEmbeddedServer;


@TestConfiguration
@Slf4j
public class AwsSqsTestConfig {
    @Value("${integrations.async.sqs.message.queue}")
    private String queueName;

    @Value("${cloud.aws.region.static}")
    private String awsRegion;

    @Bean
    @Primary
    public AmazonSQSAsync awsSqsClientEmbedded(SqsEmbeddedServer server){
        String sqsServerUrl = server.getUrl();
        server.createQueue(queueName);

        return AmazonSQSAsyncClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("x", "x")))
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(sqsServerUrl, awsRegion))
                .build();
    }

    @Bean
    public SimpleMessageListenerContainerFactory simpleMessageListenerContainerFactory(AmazonSQSAsync amazonSqs) {
        SimpleMessageListenerContainerFactory factory = new SimpleMessageListenerContainerFactory();
        factory.setWaitTimeOut(2);
        return factory;
    }

    @Bean(initMethod = "start", destroyMethod = "stop")
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public SqsEmbeddedServer getSqsServer(){
        return new SqsEmbeddedServer();
    }

}

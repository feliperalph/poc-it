package poc.integration.domain.message;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Message {
    private String words;
    private String author;
}

package poc.integration.domain.message;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import poc.integration.domain.message.external.api.swapi.SwapiService;
import poc.integration.domain.message.storage.dynamo.DynamoStorageService;
import poc.integration.domain.message.storage.rds.RdsStorageService;
import poc.integration.domain.message.storage.redis.RedisStorageService;
import poc.integration.domain.message.storage.s3.S3StorageService;

import java.util.Collection;

@Service
@RequiredArgsConstructor
@Slf4j
public class MessageService {
    private final DynamoStorageService dynamoStorageService;
    private final RdsStorageService rdsStorageService;
    private final RedisStorageService redisStorageService;
    private final S3StorageService s3StorageService;
    private final SwapiService swapiService;

    public void save(String words) {
        String author = this.swapiService.getRandomCharacter();

        Message message = Message.builder()
                .author(author)
                .words(words)
                .build();

        log.info("Saving words as message[{}] from[{}]", words, author);
        this.dynamoStorageService.save(message);
        this.rdsStorageService.save(message);
        this.redisStorageService.save(message);
        this.s3StorageService.save(message);
    }

    public Collection<Message> listDynamo() {
        return null;
    }

    public Collection<Message> listRds() {
        return null;
    }

    public Collection<Message> listRedis() {
        return null;
    }

    public Collection<Message> listS3() {
        return this.s3StorageService.listAll();
    }
}

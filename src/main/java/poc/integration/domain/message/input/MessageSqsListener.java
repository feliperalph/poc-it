package poc.integration.domain.message.input;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class MessageSqsListener {

    @SqsListener( "${integrations.async.sqs.message.queue}")
    public void receiveMessage(String message, @Header("SenderId") String senderId) {
        log.info("Receiving words[{}] from sqs", message);
    }
}

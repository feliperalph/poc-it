package poc.integration.domain.message.input;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import poc.integration.domain.message.MessageService;

@Component
@RequiredArgsConstructor
@Slf4j
public class MessageKafkaListener {
    private final MessageService messageService;

    //@KafkaListener(topics = "${integrations.async.kafka.message.topic}", groupId = "${integrations.async.kafka.message.topic.group}")
    public void listen(String message) {
        log.info("Publishing message to sqs. {}", message);
        this.messageService.save(message);
    }
}

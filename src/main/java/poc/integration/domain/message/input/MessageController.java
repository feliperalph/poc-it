package poc.integration.domain.message.input;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import poc.integration.domain.message.Message;
import poc.integration.domain.message.MessageService;
import poc.integration.domain.message.external.api.swapi.SwapiService;
import poc.integration.domain.message.output.MessageKafkaProducer;
import poc.integration.domain.message.output.MessageSqsProducer;
import poc.integration.domain.message.storage.s3.S3StorageService;

import javax.validation.constraints.NotNull;
import java.util.Collection;

@RestController
@RequiredArgsConstructor
@Slf4j
@Validated
public class MessageController {
    private final MessageService messageService;
    private final MessageKafkaProducer kafkaPublisher;
    private final MessageSqsProducer sqsPublisher;

    private final SwapiService swapiService;//TODO: remove after

    @GetMapping("/")
    public String testSwapi() {
        log.info("Call SWAPI");
        return this.swapiService.getRandomCharacter();
    }

    @PostMapping("/sqs")
    public void publishSqs(@NotNull @RequestBody String words) {
        log.info("Publishing words to sqs. {}", words);
        this.sqsPublisher.publish(words);
    }

    @PostMapping("/kafka")
    public void publishKafka(@NotNull @RequestBody String words) {
        log.info("Publishing words to kafka. {}", words);
        this.kafkaPublisher.publish(words);
    }

    @GetMapping(value = "/dynamo", produces = "application/json")
    public Collection<Message> listAllDynamo() {
        Collection<Message> messages = messageService.listDynamo();
        log.info("Listing all messages from dynamo. Total={}", messages.size());
        return messages;
    }

    @GetMapping(value = "/redis", produces = "application/json")
    public Collection<Message> listAllRedis() {
        Collection<Message> messages = messageService.listRedis();
        log.info("Listing all messages from redis. Total={}", messages.size());
        return messages;
    }

    @GetMapping(value = "/rds", produces = "application/json")
    public Collection<Message> listAllRds() {
        Collection<Message> messages = messageService.listRds();
        log.info("Listing all messages from rds. Total={}", messages.size());
        return messages;
    }

    @GetMapping(value = "/s3", produces = "application/json")
    public Collection<Message> listAllS3() {
        Collection<Message> messages = messageService.listS3();
        log.info("Listing all messages from s3. Total={}", messages.size());
        return messages;
    }

}

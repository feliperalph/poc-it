package poc.integration.domain.message.external.api.swapi;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "${integrations.sync.api.swapi.name}", url = "${integrations.sync.api.swapi.url}")
public interface SwapiClient {
    @GetMapping(value = "/${integrations.sync.api.swapi.resource}/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    Character getCharacterById(@PathVariable("id") int characterId);
}

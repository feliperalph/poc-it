package poc.integration.domain.message.external.api.swapi;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonDeserialize(builder = Character.CharacterBuilder.class)
public class Character {

    private int id;

    private String name;

    private String gender;

    @JsonPOJOBuilder(withPrefix = "")
    public static final class CharacterBuilder {
    }
}

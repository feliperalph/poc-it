package poc.integration.domain.message.external.api.swapi;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.concurrent.ThreadLocalRandom;

@Service
@RequiredArgsConstructor
@Slf4j
public class SwapiService {
    @Value("${integrations.sync.api.swapi.total}")
    private int totalChars;

    @Value("${integrations.sync.api.swapi.api-name}")
    private String apiName;

    private final SwapiClient client;

    public String getRandomCharacter() {
        var id = ThreadLocalRandom.current().nextInt(1, totalChars + 1);
        Character character = this.client.getCharacterById(id);
        character.setId(id);
        log.info("Requested {} {}", this.apiName, character);
        return character.getName();
    }
}

package poc.integration.domain.message.output;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class MessageSqsProducer implements MessageProducer {

    @Value("${integrations.async.sqs.message.queue}")
    private String queueName;

    private final QueueMessagingTemplate messagingTemplate;;

    @Override
    public void publish(String words) {
        messagingTemplate.convertAndSend(queueName, words);
        log.info("Published words[{}] to sqs", words);
    }
}

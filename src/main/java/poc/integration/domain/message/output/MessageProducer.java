package poc.integration.domain.message.output;

public interface MessageProducer {
    void publish(String words);
}

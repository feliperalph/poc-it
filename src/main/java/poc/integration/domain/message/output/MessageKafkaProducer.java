package poc.integration.domain.message.output;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MessageKafkaProducer implements MessageProducer {
    @Override
    public void publish(String words) {
        log.info("Publishing words[{}] to kafka", words);
    }
}

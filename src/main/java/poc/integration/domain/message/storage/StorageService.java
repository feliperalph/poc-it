package poc.integration.domain.message.storage;

import poc.integration.domain.message.Message;

import java.util.Collection;

public interface StorageService {
    Message save(Message message);

    Collection<Message> listAll();
}

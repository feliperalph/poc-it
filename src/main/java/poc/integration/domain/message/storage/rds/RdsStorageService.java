package poc.integration.domain.message.storage.rds;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import poc.integration.domain.message.Message;
import poc.integration.domain.message.storage.StorageService;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public class RdsStorageService implements StorageService {
    private final MySqlRepository repository;

    @Override
    public Message save(Message message) {
        return null;
    }

    @Override
    public Collection<Message> listAll() {
        return null;
    }
}

package poc.integration.domain.message.storage.s3;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import poc.integration.domain.message.Message;
import poc.integration.domain.message.storage.StorageService;

import java.util.Collection;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class S3StorageService implements StorageService {
    private final S3Repository repository;

    @Override
    public Message save(Message message) {
        return null;
    }

    @Override
    public Collection<Message> listAll() {
        return repository.listBuckets().stream()
                .map(name -> Message.builder()
                        .words(name)
                        .build())
                .collect(Collectors.toSet());
    }
}

package poc.integration.domain.message.storage.redis;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import poc.integration.domain.message.Message;
import poc.integration.domain.message.storage.StorageService;

import java.util.Collection;

@Service
@RequiredArgsConstructor
public class RedisStorageService implements StorageService {
    private final RedisRepository repository;

    @Override
    public Message save(Message message) {
        return null;
    }

    @Override
    public Collection<Message> listAll() {
        return null;
    }
}

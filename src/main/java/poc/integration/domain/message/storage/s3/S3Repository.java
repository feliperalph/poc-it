package poc.integration.domain.message.storage.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Slf4j
class S3Repository {
    private final AmazonS3 s3Client;

    public Collection<String> listBuckets(){
        List<Bucket> buckets = s3Client.listBuckets();
        log.info("Your Amazon S3 buckets are: {}", buckets);

        return buckets.stream()
                .map(Bucket::getName)
                .collect(Collectors.toList());
    }
}

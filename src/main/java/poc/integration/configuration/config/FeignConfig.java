package poc.integration.configuration.config;

import feign.okhttp.OkHttpClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "poc.integration.domain.message.external.api")
public class FeignConfig {
    @Bean
    public OkHttpClient client() {
        return new OkHttpClient();
    }
}
